#ifndef GRAVITY_TANK
#define GRAVITY_TANK

#include <iostream>
#include <ostream>

template <class T>
struct GravityTank {
    T* storage;
    int count;
    int capacity;

    GravityTank() {
        count = 0;
        capacity = 1;
        storage = new T[capacity];
    }

    GravityTank(const GravityTank<T>& other) {
        std::cout << "copy constructor" << std::endl;
        count = other.count;
        capacity = other.capacity;

        storage = new T[capacity];
        for (int i = 0; i < count; i++) {
            storage[i] = other.storage[i];
        }
    }

    GravityTank<T>& operator=(const GravityTank<T>& other) {
        std::cout << "overloaded assignment operator" << std::endl;

        if (capacity == other.capacity) {
            count = other.count;
            for (int i = 0; i < count; i++) {
                storage[i] = other.storage[i];
            }
        } else {
            delete[] storage;
            count = other.count;
            capacity = other.capacity;
            
            storage = new T[capacity];

            for (int i = 0; i < count; i++) {
                storage[i] = other.storage[i];
            }
        }

        return *this;
    }

    bool operator>(const GravityTank<T>& other) {
        return count > other.count;
    }

    void append(T x) {
        storage[count] = x;
        count++;

        if (count == capacity) {
            capacity *= 2;

            T* temp = new T[capacity];
            for (int i = 0; i < count; i++) {
                temp[i] = storage[i];
            }

            T* old = storage;
            storage = temp;
            delete[] old;
        }

        int curr = count - 1;
        while (curr > 0 && storage[curr - 1] > storage[curr]) {
            T temp = storage[curr];
            storage[curr] = storage[curr - 1];
            storage[curr - 1] = temp;
            curr--;
        }
    }

    ~GravityTank() {
        delete[] storage;
    }
};

template <class T>
std::ostream& operator<<(std::ostream& os, const GravityTank<T>& tank) {
    os << "Gravity tank: ";
    for (int i = 0; i < tank.count; i++) {
        os << tank.storage[i];
        if (i < tank.count - 1) {
            os << ", ";
        }
    }

    return os;
}

#endif