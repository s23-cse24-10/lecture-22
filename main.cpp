#include <cstdint>
#include <iostream>
#include "gravityTank.h"
using namespace std;

int main() {
    GravityTank<int> tank;
    tank.append(5);
    tank.append(7);
    tank.append(3);

    GravityTank<int> tank2;
    tank2.append(4);
    tank2.append(2);

    GravityTank<GravityTank<int>> bigTank;
    bigTank.append(tank);
    bigTank.append(tank2);
    cout << bigTank << endl;

    return 0;
}